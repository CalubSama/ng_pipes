import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ImagesPipe } from './pages/images.pipe';
import { ImagesComponent } from './pages/images/images.component';

@NgModule({
  declarations: [
    AppComponent,
    ImagesPipe,
    ImagesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
